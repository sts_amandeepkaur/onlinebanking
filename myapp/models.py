from django.db import models
import random
import datetime
from django.contrib.auth.models import User

class register(models.Model):
    title_choices=(
        ("Mr", "Mr"),
        ("Miss", "Miss"),
        ("Mrs", "Mrs"),
    )
    account_choices=(
        ("Saving","Saving"),
        ("Current","Current"),
    )
    user =models.OneToOneField(User,on_delete =models.CASCADE,null =True)
    title= models.CharField(max_length=10,choices=title_choices,default="Mr",null=True)
    account_types=models.CharField(max_length=15,choices=account_choices,default="Saving")
    account_balance=models.IntegerField(null=True)
    accunt_num=models.IntegerField(unique=True,null=True)
    profile_picture=models.ImageField(upload_to='profile/%Y/%m/%d',null =True)

    def __str__(self):
        return self.user.username

# Create your models here.
class transactions(models.Model):
    sender=models.CharField(max_length=200,null=True)
    receiver=models.CharField(max_length=200,null=True)
    amount=models.IntegerField(null=True)
    time_transtaction=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return repr(self.time_transtaction)

class notification(models.Model):
    user_id = models.IntegerField()
    status= models.BooleanField(default=False)
    message=models.CharField(max_length=500)
    action_on = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return str(self.message)

class contactus(models.Model):
    name =  models.CharField(max_length=250,blank=True)
    email =  models.EmailField(max_length=250,blank=True)
    phone =  models.IntegerField(blank=True)
    msz = models.TextField(blank=True)
    on_date = models.DateField(auto_now_add=True)
    on_time = models.TimeField(auto_now_add=True)

    def __str__(self):
        return self.name